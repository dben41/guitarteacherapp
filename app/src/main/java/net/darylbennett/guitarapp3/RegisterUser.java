package net.darylbennett.guitarapp3;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import java.util.concurrent.ExecutionException;

/**
 * Created by Daryl on 12/8/2014.
 */
public class RegisterUser extends Activity {
    Boolean valid = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final Intent mainSreenIntent = new Intent(this, MainActivity.class);
        mainSreenIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        setContentView(R.layout.create_user);

        //format the action bar better
        getActionBar().setTitle("Register New User");
        getActionBar().setIcon(android.R.color.transparent);

        //get button
        final Button signInButton =  (Button)findViewById(R.id.signin_button3);

        //if user clicks on the help icon, display help menu
        final EditText classId = (EditText)findViewById(R.id.createUser_classId);
        classId.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (classId.getRight() - classId.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        //Log.d("hello", "world");
                        msgBox("What's the class id?.", "The class id is given to you by your instructor. Contact them to get it.");
                        return true;
                    }
                }
                return false;
            }
        });

        //Verify user credentials
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //ensure the necessary fields are filled in
                if(validateFormFields())
                    startActivityForResult(mainSreenIntent, 0);
            }
        });



    }

    public void msgBox(String messageHeader, String message)
    {
        // Show popup dialog box
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_info)
                .setTitle(messageHeader)
                .setMessage(message)
                .setNeutralButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .show();
    }

    /**
     * Verifies the data is correct
     * @return
     */
    public Boolean validateFormFields()
    {
        //get all the fields
        EditText emailField = (EditText)findViewById(R.id.createUser_email);
        EditText usernameField = (EditText)findViewById(R.id.createUser_username);
        EditText passwordField = (EditText)findViewById(R.id.createUser_password);
        EditText phoneField = (EditText)findViewById(R.id.createUser_phone);
        EditText classIdField = (EditText)findViewById(R.id.createUser_classId);
        EditText fullNameField = (EditText)findViewById(R.id.createUser_fullName);
        CheckBox createAdmin = (CheckBox)findViewById(R.id.createAdmin);

        //get all the strings of all the fields
        String email = emailField.getText().toString();
        String user = usernameField.getText().toString();
        String password = passwordField.getText().toString();
        String phone = phoneField.getText().toString();
        String classId = classIdField.getText().toString();
        String fullName = fullNameField.getText().toString();
        Boolean isAdmin = createAdmin.isChecked();

        //checks if the fields are empty
        if(email.equals("") || user.equals("") || password.equals("") || phone.equals("") || classId.equals("") || fullName.equals(""))
        {
            msgBox("Error.", "Please fill in all the fields!");
            return false;
        }

        /* Here insert various methods to verify format */
        //check email
        //check classId
        //check number

        //all the data is good
        if(createUser(email, user, password, phone, classId, fullName, isAdmin)) {
            msgBox("Success.", "Successfully registered!");
            return true;
        }else {
            msgBox("Error.", "Username already used! Please pick another.");
            return false;
        }
    }

    /**
     * This creates a user
     * @param username
     * @param password
     * @return
     */
    public Boolean createUser(final String email, final String username, final String password, final String phone, final String classId, final String name, final Boolean isAdmin){
        valid = false;
        AsyncTask<String, Integer, Boolean> createUser = new AsyncTask<String, Integer, Boolean>() {
            @Override
            protected Boolean doInBackground(String... params) {
                return RestInterface.CreateUser(email, username, password, phone, classId, name, isAdmin);
            }

            @Override
            protected void onPostExecute(Boolean aBoolean) {
                super.onPostExecute(aBoolean);
                valid = aBoolean;
            }
        };

        try {
            valid =  createUser.execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return valid;
    }


}


