package net.darylbennett.guitarapp3;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Fragment;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Daryl on 12/11/2014.
 */
public class SettingsFragment extends Fragment {
    AlarmManager alarmManager;
    PendingIntent pendingIntent;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //return super.onCreateView(inflater, container, savedInstanceState);

        //set the notification


        View rootView = inflater.inflate(R.layout.settings_page, container, false);

        //get radio button
        final CheckBox lessons = (CheckBox) rootView.findViewById(R.id.settingsLessonRadioButton);
        Button submit = (Button)rootView.findViewById(R.id.settings_apply);

        //click listener
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(lessons.isChecked())
                    createNotification();
                else
                    stopNotification();
            }
        });

        return rootView;

    }

    /**
     * This is a very basic hard-coded.
     * Some-day it could take you to your lesson page.
     */
    public void createNotification() {
        // Prepare intent which is triggered if the
        // notification is selected
        Intent intent = new Intent(getActivity(), NotificationPublisher.class);

        // Build notification
        // Actions are just fake
        Notification noti = new Notification.Builder(getActivity())
                .setContentTitle("Reminder")
                .setContentText("Don't forget your weekly lesson at 6:00pm, Thursday! Remember to practice! :) ").setSmallIcon(R.drawable.ic_launcher)
                .build();
        noti.flags |= Notification.FLAG_AUTO_CANCEL;


        intent.putExtra("notification_id", 1);
        intent.putExtra("notification", noti);

        pendingIntent = PendingIntent.getBroadcast(getActivity(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        //long futureInMillis = SystemClock.elapsedRealtime() + 1000; //for testing
        alarmManager = (AlarmManager)getActivity().getSystemService(Context.ALARM_SERVICE);

        //weekly time
        int week = 604800000;

        //start date
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Calendar calendar = Calendar.getInstance();
        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);

        //default for demo will be wednesday
        int diff = Calendar.WEDNESDAY - dayOfWeek;
        if(!(diff > 0))
            diff += 7;
        Calendar date = Calendar.getInstance();
        date.add(Calendar.DAY_OF_MONTH, diff);
//        long i = date.getTimeInMillis();

//        int year       = calendar.get(Calendar.YEAR);
//        int month      = calendar.get(Calendar.MONTH); // Jan = 0, dec = 11
//        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
//        int dayOfWeeks  = calendar.get(Calendar.DAY_OF_WEEK);
//        int weekOfYear = calendar.get(Calendar.WEEK_OF_YEAR);
//        int weekOfMonth= calendar.get(Calendar.WEEK_OF_MONTH);
//
//        int hour       = calendar.get(Calendar.HOUR);        // 12 hour clock
//        int hourOfDay  = calendar.get(Calendar.HOUR_OF_DAY); // 24 hour clock
//        int minute     = calendar.get(Calendar.MINUTE);
//        int second     = calendar.get(Calendar.SECOND);
//        int millisecond= calendar.get(Calendar.MILLISECOND);
//        date.setTime();
//        dateFormat.format(date);


        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, date.getTimeInMillis() ,week, pendingIntent);

    }

    private void stopNotification(){
        if(alarmManager != null)
            alarmManager.cancel(pendingIntent);
    }

}
