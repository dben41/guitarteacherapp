package net.darylbennett.guitarapp3;

import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ExecutionException;

/**
 * Created by Daryl on 12/20/2014.
 */
public class AddLessonFragment extends Fragment{
    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    int uid;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.add_student, container, false);

        //when submit button is clicked
        Button button = (Button) rootView.findViewById(R.id.add_lesson_button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //get the content
                EditText content = (EditText) getActivity().findViewById(R.id.content_textField);
                String contentString = content.getText().toString();

                //get the date and get it to string
                String date = new SimpleDateFormat("dd-MM-yyyy").format(new Date());

                //default
                EditText uidText = (EditText) getActivity().findViewById(R.id.uid_textField);
                uid  = Integer.parseInt(uidText.getText().toString());

                //add a data constraint if i had time
                //add to database
                addLesson(date, contentString,uid);

                //redirect
                Toast.makeText(getActivity(), "Lesson Inserted Successfully.", Toast.LENGTH_SHORT).show();
                returnToStudentViewListener.returnEvent();

            }
        });


        return rootView;
    }

    public void addLesson(final String date, final String content, final int uid){
        AsyncTask<String, Integer, Boolean> getUserTask = new AsyncTask<String, Integer, Boolean>() {
            @Override
            protected Boolean doInBackground(String... params) {
                RestInterface.addLesson(uid,date,content);
                return true;
            }
        };

        getUserTask.execute();
        //return user;
    }

    //listener stuff, to open student activity
    public interface ReturnToStudentViewListener{
        public void returnEvent();
    }

    ReturnToStudentViewListener returnToStudentViewListener = null;
    public ReturnToStudentViewListener getReturnToStudentViewListener(){return returnToStudentViewListener;}
    public void setReturnToStudentViewListener(ReturnToStudentViewListener listener){
        this.returnToStudentViewListener = listener;
    }

}
