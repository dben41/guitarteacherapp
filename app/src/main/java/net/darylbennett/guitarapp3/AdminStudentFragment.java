package net.darylbennett.guitarapp3;

import android.app.Fragment;
import android.app.FragmentManager;
import android.database.DataSetObserver;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

/**
 * Created by Daryl on 12/16/2014.
 */
public class AdminStudentFragment extends Fragment implements ListAdapter {
    //get a list of all the users from the remote server
    ArrayList<User> allUsers;
    ListView studentListView;
    private UUID[] orderedStudents = null;

    //empty constructor
    public AdminStudentFragment(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //contains all the views
        LinearLayout rootLayout= new LinearLayout(getActivity());
        rootLayout.setOrientation(LinearLayout.VERTICAL);
        //Tell the admin info about this view. In the future, add users here? show number of students?
        TextView infoView = new TextView(getActivity());
        infoView.setText("Click on a User to view their lessons.");
        infoView.setBackgroundColor(Color.rgb(0x23,0xB5,0xE5));

        //instantiae the listView and set the adapter0
        studentListView = new ListView(getActivity());
        studentListView.setAdapter(this);

        //set on click stuff
        studentListView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
               // if(onLessonSelectedListener != null){
                    //onLessonSelectedListener.onLessonSelected(AdminStudentFragment.this, position);
                    Log.v("Postion", position+"");
                //}
            }
        });

        Button addButton = new Button(getActivity());
        addButton.setText("Add Lesson");
        //have the button open a new activity to add a lesson
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addLessonListener.addNewLesson();
            }
        });

        //add the views to the layout and return
        rootLayout.addView(infoView, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0, 15));
        rootLayout.addView(addButton);
        rootLayout.addView(studentListView, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0, 85));


        return rootLayout;

        //return infoView;
    }


    public ArrayList<User> getUsers(final String classId){
        ArrayList<User> adminUsers;
        RestInterface.UserList userListl;
        userListl = null;
        adminUsers = null;
        AsyncTask<String, Integer, RestInterface.UserList> createUser = new AsyncTask<String, Integer, RestInterface.UserList>() {
            @Override
            protected RestInterface.UserList doInBackground(String... params) {
                return RestInterface.getClassUsers(classId);
            }
        };

        try {
            userListl =  createUser.execute().get();
            adminUsers = new ArrayList<User>();
            for(RestInterface.TempUser temp : userListl.users){
                adminUsers.add(new User(temp.email, null, temp.phone, null, temp.name, 0, temp.uid));
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return adminUsers;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return true;
    }

    @Override
    public boolean isEnabled(int i) {
        return false;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver dataSetObserver) {

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver dataSetObserver) {

    }

    /**
     * Loads all the users every time this is called. I could create refresh button
     * @return
     */
    @Override
    public int getCount() {
//        if(allUsers == null)
//        {
          //get the class id from the admin
          String class_id = AppModel.getInstance().getUser().class_id;
          allUsers   = getUsers(class_id);
//        }
        return allUsers.size();
    }

    @Override
    public Object getItem(int i) {
        return allUsers.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        int userCount = this.getCount();
        final int count = i;

        LinearLayout root = new LinearLayout(getActivity());
        root.setOrientation(LinearLayout.VERTICAL);

        //Set the text of all the views
        TextView name = new TextView(getActivity());
        name.setText(allUsers.get(i).name);

        TextView phone = new TextView(getActivity());
        phone.setText(allUsers.get(i).phone);

        TextView email = new TextView(getActivity());
        email.setText(allUsers.get(i).email);

        //add to the root layout
        root.addView(name);
        root.addView(phone);
        root.addView(email);

        root.setOnClickListener(new View.OnClickListener() {
            @Override//TODO
             public void onClick(View view) {
                Log.v("Item " + count, "This item was touched!");

                User u = allUsers.get(count);
                int uid = u.uid;
                //String string = AppModel.getInstance().getUser().username;
                studentSelectedListener.startNewStudent(uid);
            }
        });

        return root;
    }

    @Override
    public int getItemViewType(int i) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean isEmpty() {
        return this.getCount()<=0;
    }


    StudentSelectedListener studentSelectedListener = null;
    public StudentSelectedListener getStudentSelectedListener(){return studentSelectedListener;}
    public void setStudentSelectedListener(StudentSelectedListener listener){
        this.studentSelectedListener = listener;
    }

    //listener stuff, to open student activity
    public interface StudentSelectedListener{
        public void startNewStudent(int uid);
    }

    public AddLessonListener getAddLessonListener() {
        return addLessonListener;
    }

    public void setAddLessonListener(AddLessonListener addLessonListener) {
        this.addLessonListener = addLessonListener;
    }

    AddLessonListener addLessonListener = null;

    public interface AddLessonListener{
        public void addNewLesson();
    }

    //addLessonListener.addNewLesson();
}

