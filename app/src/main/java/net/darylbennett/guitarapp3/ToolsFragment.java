package net.darylbennett.guitarapp3;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by Daryl on 12/11/2014.
 */
public class ToolsFragment extends Fragment{
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //return super.onCreateView(inflater, container, savedInstanceState);
        //format the action bar better
        //getActionBar().setTitle("Register New User");
        //getActionBar().setIcon(android.R.color.transparent);

        //get button

        View rootView = inflater.inflate(R.layout.tools_menu, container, false);

        //image
        ImageView guitarTuner = (ImageView) rootView.findViewById(R.id.toolsfragment_guitartuner);
        guitarTuner.setImageResource(R.drawable.tuner);
        guitarTuner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String appPackageName = "com.symbolic.pitchlab&hl=en";
                Intent guitarTuner = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName));
                try {
                    startActivity(guitarTuner);
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }
        });


        ImageView metronome = (ImageView) rootView.findViewById(R.id.toolsfragment_metronome);
        metronome.setImageResource(R.drawable.metronome);
        metronome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String appPackageName = "com.andymstone.metronome&hl=en";
                Intent guitarTuner = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName));
                try {
                    startActivity(guitarTuner);
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }
        });

        ImageView tabs = (ImageView) rootView.findViewById(R.id.toolsfragment_tabs);
        tabs.setImageResource(R.drawable.tabs);
        tabs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String appPackageName = "org.xssembler.chordsplus&hl=en";
                Intent guitarTuner = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName));
                try {
                    startActivity(guitarTuner);
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }
        });


        return rootView;

    }
}
