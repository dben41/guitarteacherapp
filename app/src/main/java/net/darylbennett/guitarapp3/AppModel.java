package net.darylbennett.guitarapp3;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Daryl on 12/11/2014.
 */
public class AppModel {
    //underlying user
    User user = null;
    //student list
    ArrayList<Lesson> studentLessonList = null; //new ArrayList<Lesson>();
    //jif admin, have all the lessons of your students
    HashMap<Integer, ArrayList<Lesson>> allStudentList = null;//new HashMap<Integer, ArrayList<Lesson>>();



    static AppModel instance = null;
    //singleton instance
    static AppModel getInstance()
    {
        if(instance == null)
            instance = new AppModel();
        return instance;
    }

    //empty constructor
    private AppModel(){}

    //set user
    public void setUser(String email, String username, String phone, String classId, String fullName, int uid)
    {
        if(user==null)
            user = new User();
        user.email = email;
        user.username = username;
        user.phone = phone;
        user.class_id = classId;
        user.name = fullName;
        user.uid = uid;
    }

    public void setUser(User user1)
    {
        if(user==null)
            user = new User();
        this.user.email = user1.email;
        this.user.username = user1.username;
        this.user.phone = user1.phone;
        this.user.class_id = user1.class_id;
        this.user.name = user1.name;
        this.user.uid = user1.uid;
    }
    //get user
    public User getUser()
    {
        if(user == null)
            return null;
        else
            return user;
    }

    public void setStudentLessonList(ArrayList<Lesson> studentList)
    {
        studentLessonList = studentList;
        //delete admin list
        allStudentList = null;
    }

    public void setAllStudentList(HashMap<Integer, ArrayList<Lesson>> allStudentList) {
        this.allStudentList = allStudentList;
        studentLessonList = null;
    }

    public ArrayList<Lesson> getStudentLessonList() {
        return studentLessonList;
    }

    public HashMap<Integer, ArrayList<Lesson>> getAllStudentList() {
        return allStudentList;
    }

    /*
     * Instructor can add a lesson
     */
    public void addLesson(String username, String content, String date, int uid ){
        //find the list of lessons of the students
        ArrayList<Lesson> temp = allStudentList.get(uid);
        //create a temp lesson
        Lesson tempLesson = new Lesson(username,content,date,uid);
        //add to that student's less list
        temp.add(tempLesson);
        //replace the list of lessons
        allStudentList.put(uid,temp);
    }


}
