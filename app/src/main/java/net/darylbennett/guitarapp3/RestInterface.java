package net.darylbennett.guitarapp3;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONStringer;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Contains a set of methods that communicate with the RESTful remote server.
 * Created by Daryl on 12/8/2014.
 */
public class RestInterface {
    //public static final String BASE_URL = "http://localhost:8080/";
    public static final String BASE_URL = "http://10.0.2.2:8080/";
    //public static final String BASE_URL = "http://54.149.90.121/";
    /**
     * Represents a user
     */
//    public class User{
//        int id;
//        String username;
//        String password;
//        int role;
//        String class_id;
//        String phone;
//        String name;
//    }
    public class UserList{
        TempUser[] users;
    }

    public class TempUser{
        int uid;
        String name;
        String phone;
        String email;
    }

    public class Lesson{
        int uid;
        String date;//may change
        String content;
    }

    public class LessonList{
        Lesson[] lessons;
    }


    /**
     * I wouldn't pass around an unencrypted password
     */
    public static Boolean SignIn(String username, String password)
    {
        Boolean b = false;
        try {
            //connect
            HttpClient client =  new DefaultHttpClient();
            HttpPost   post   = new HttpPost(BASE_URL + "user/login");

            //Params for request
            // Request parameters and other properties.
            // List<NameValuePair> params = new ArrayList<NameValuePair>(2);
            // params.add(new BasicNameValuePair("username", username));
            // params.add(new BasicNameValuePair("password", password));
            //  post.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
            JSONStringer vm = null;
            try {
                vm = new JSONStringer().object().key("username")
                        .value(username).key("password").value(password)
                        .endObject();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            //String jsonString = "'{\"username\": \"user1\",\"password\": \"123\"}'";
            post.setEntity(new StringEntity(vm.toString(), "UTF-8"));
            post.setHeader("Content-type", "application/json");

            //execute
            HttpResponse response = client.execute(post);

            //get response
            InputStream responseContent = response.getEntity().getContent();
            Scanner responseScanner = new Scanner(responseContent).useDelimiter("\\A");
            String responseString = responseScanner.hasNext() ? responseScanner.next() : null;

            //error checkre
            if (responseString == null)
                return  null;
            if(responseString.contains("Signin successful!")) {
                b = true;
            }
            else
                b = false;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return b;
    }

    /**
     * I wouldn't pass around an unencrypted password
     */
    public static void addLesson(int uid, String date, String content)
    {
        Boolean b = false;
        try {
            //connect
            HttpClient client =  new DefaultHttpClient();
            HttpPost   post   = new HttpPost(BASE_URL + "lesson/add");

            JSONStringer vm = null;
            try {
                vm = new JSONStringer().object().key("uid")
                        .value(uid).key("date").value(date)
                        .key("content").value(content)
                        .endObject();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            //String jsonString = "'{\"username\": \"user1\",\"password\": \"123\"}'";
            post.setEntity(new StringEntity(vm.toString(), "UTF-8"));
            post.setHeader("Content-type", "application/json");


            //execute
            HttpResponse response = client.execute(post);
            //get response
            InputStream responseContent = response.getEntity().getContent();
            Scanner responseScanner = new Scanner(responseContent).useDelimiter("\\A");
            String responseString = responseScanner.hasNext() ? responseScanner.next() : null;

            if(responseString == "")
                b= true;

            return;


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     */
    public static User getUser(int id){
        try {
            //connect
            HttpClient client = new DefaultHttpClient();
            HttpGet get= new HttpGet(BASE_URL + "user/" + id);

            //execute
            HttpResponse response = client.execute(get);

            //get response
            InputStream responseContent = response.getEntity().getContent();
            Scanner responseScanner = new Scanner(responseContent).useDelimiter("\\A");
            String responseString = responseScanner.hasNext() ? responseScanner.next() : null;

            //error check
            if( responseString == null)
                return null;

            //convert to JSON String into an object
            Gson gson = new Gson();
            Type type = new TypeToken<User>() {}.getType();
            User user = gson.fromJson(responseString, type);
            return user;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static User getUser(String username){
        User user = null;
        try {
            //connect
            HttpClient client = new DefaultHttpClient();
            HttpGet get= new HttpGet(BASE_URL + "user/username/" + username);

            //execute
            HttpResponse response = client.execute(get);

            //get response
            InputStream responseContent = response.getEntity().getContent();
            Scanner responseScanner = new Scanner(responseContent).useDelimiter("\\A");
            String responseString = responseScanner.hasNext() ? responseScanner.next() : null;

            //error check
            if( responseString == null)
                return null;

            //convert to JSON String into an object
            Gson gson = new Gson();
            Type type = new TypeToken<User>() {}.getType();
            user = gson.fromJson(responseString, type);
            return user;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return user;
    }

    /**
     * Returns the number of users a teacher has.
     * @param classId
     * @return
     */
    public static int classCount(String classId){
        int count = 0;
        try {
            //connect
            HttpClient client = new DefaultHttpClient();
            HttpGet get= new HttpGet(BASE_URL + "user/classCount/" + classId);

            //execute
            HttpResponse response = client.execute(get);

            //get response
            InputStream responseContent = response.getEntity().getContent();
            Scanner responseScanner = new Scanner(responseContent).useDelimiter("\\A");
            String responseString = responseScanner.hasNext() ? responseScanner.next() : null;

            //error check
            if( responseString == null)
                return 0;

            //convert to JSON String into an object
            Gson gson = new Gson();
            count = gson.fromJson(responseString, Integer.class);
            return count;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return count;
    }

    /**
         * Returns the number of users a teacher has.
         * @param classId
         * @return
         */
    public static UserList getClassUsers(String classId){
        UserList user = null;
        try {
            //connect
            HttpClient client = new DefaultHttpClient();
            HttpGet get= new HttpGet(BASE_URL + "user/class/" + classId);

            //execute
            HttpResponse response = client.execute(get);

            //get response
            InputStream responseContent = response.getEntity().getContent();
            Scanner responseScanner = new Scanner(responseContent).useDelimiter("\\A");
            String responseString = responseScanner.hasNext() ? responseScanner.next() : null;

            //error check
            if( responseString == null)
                return null;

            //convert to JSON String into an object
            Gson gson = new Gson();
            Type type = new TypeToken<UserList>() {}.getType();
            user = gson.fromJson(responseString, type);
            return user;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return user;
    }


    public static Boolean CreateUser(String email, String username, String password, String phone, String class_id, String name, Boolean isAdmin)
    {
        Boolean b = false;
        try {
            //connect
            HttpClient client =  new DefaultHttpClient();
            HttpPost   post   = new HttpPost(BASE_URL + "user/add");

            //Params for request
            // Request parameters and other properties.
            JSONStringer vm = null;
            try {
                vm = new JSONStringer().object()
                        .key("email").value(email)
                        .key("username").value(username)
                        .key("password").value(password)
                        .key("phone").value(phone)
                        .key("class_id").value(class_id)
                        .key("name").value(name)
                        .key("role").value(isAdmin)
                        .endObject();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            //String jsonString = "'{\"username\": \"user1\",\"password\": \"123\"}'";
            post.setEntity(new StringEntity(vm.toString(), "UTF-8"));
            post.setHeader("Content-type", "application/json");

            //execute
            HttpResponse response = client.execute(post);

            //get response
            InputStream responseContent = response.getEntity().getContent();
            Scanner responseScanner = new Scanner(responseContent).useDelimiter("\\A");
            String responseString = responseScanner.hasNext() ? responseScanner.next() : null;

            //error checkre
            if (responseString == null)
                return  null;
            if(responseString.contains("User inserted successfully!"))
                b = true;
            else
                b = false;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return b;
    }

    /**
     * Returns the number of users a teacher has.
     * @returnget
     */
    public static LessonList getLessonList(int uid) {
        LessonList lessons = null;
        try {
            //
            HttpClient client = new DefaultHttpClient();
            HttpGet get = new HttpGet(BASE_URL + "lesson/" + uid);

            //execute
            HttpResponse response = client.execute(get);

            //get response
            InputStream responseContent = response.getEntity().getContent();
            Scanner responseScanner = new Scanner(responseContent).useDelimiter("\\A");
            String responseString = responseScanner.hasNext() ? responseScanner.next() : null;

            //error check
            if (responseString == null || responseString.contains("Bad Request!"))
                return null;

            //convert to JSON String into an object
            Gson gson = new Gson();
            Type type = new TypeToken<LessonList>() {}.getType();
            lessons = gson.fromJson(responseString, type);
            return lessons;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return lessons;
    }
}
