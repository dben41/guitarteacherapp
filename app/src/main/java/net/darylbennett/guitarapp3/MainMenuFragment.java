package net.darylbennett.guitarapp3;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by Daryl on 12/10/2014.
 */
public class MainMenuFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //return super.onCreateView(inflater, container, savedInstanceState);

        View rootView = inflater.inflate(R.layout.main_menu, container, false);

        //Welcome user by name
        TextView name = (TextView) rootView.findViewById(R.id.MainMenu_name);
        String s = AppModel.getInstance().getUser().name;
        name.setText("Welcome, " + s + "!");

        //show the user's stats
        TextView username = (TextView) rootView.findViewById(R.id.MainMenu_username);
        s = AppModel.getInstance().getUser().username;
        username.setText("Username: " + s);

        //class_id
        TextView class_id = (TextView) rootView.findViewById(R.id.MainMenu_class_id);
        s = AppModel.getInstance().getUser().class_id;
        class_id.setText("Your class: " + s);

        //phone
        TextView phone = (TextView) rootView.findViewById(R.id.MainMenu_phone);
        s = AppModel.getInstance().getUser().phone;
        phone.setText("Phone: " + s);

        //email
        TextView email = (TextView) rootView.findViewById(R.id.MainMenu_email);
        s = AppModel.getInstance().getUser().email;
        email.setText("Email: " + s);

        //class time

        return rootView;

    }
}
