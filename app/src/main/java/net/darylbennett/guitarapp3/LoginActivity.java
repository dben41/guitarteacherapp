package net.darylbennett.guitarapp3;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.concurrent.ExecutionException;

/**
 * Created by Daryl on 12/8/2014.
 */
public class LoginActivity extends Activity {
    Boolean valid;
    User user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.login);

        //format the action bar better
        getActionBar().setTitle("Sign In");
        getActionBar().setIcon(android.R.color.transparent);

        //get button
        final Button signInButton =  (Button)findViewById(R.id.signin_button2);

        //Verify user credentials
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Get the text from the text boxes
                EditText username = (EditText)findViewById(R.id.username);
                EditText password = (EditText)findViewById(R.id.password);
                String usernameString = username.getText().toString();
                String passwordString = password.getText().toString();
                //Boolean b = signIn(username.getText().toString(), password.getText().toString());

                if(usernameString.equals("") || passwordString.equals(""))
                    msgBox("Error.", "Please fill in all the fields!");
                    //check to see if success
                else if(signIn(username.getText().toString(), password.getText().toString())) {
                    //show user a success message
                    msgBox("Success.", "Successfully signed in!");

                    //Have user object save
                    User tempUser = getUser(username.getText().toString());
                    AppModel.getInstance().setUser(tempUser); //TODO

                    //TODO
                    //Check the user's role, redirect based on if USER or admin
                    if(tempUser.role==1)
                    {
                        //redirect to the main app
                        final Intent teacher = new Intent(v.getContext(), AdminMainActivity.class);
                        teacher.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivityForResult(teacher, 0);
                    } else if(tempUser.role==0){
                        //redirect to the main app
                        final Intent student = new Intent(v.getContext(), StudentActivity.class);
                        student.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivityForResult(student, 0);
                    }
                    //failure
                }else
                    msgBox("Failure.", "Incorrect credentials, please try again. ");
            }
        });



    }

    public Boolean signIn(final String username, final String password){
        valid = false;
        AsyncTask<String, Integer, Boolean> getSignIn = new AsyncTask<String, Integer, Boolean>() {
            @Override
            protected Boolean doInBackground(String... params) {
                return RestInterface.SignIn(username, password);
            }

            @Override
            protected void onPostExecute(Boolean aBoolean) {
                super.onPostExecute(aBoolean);
                valid = aBoolean;
            }
        };

        try {
            valid =  getSignIn.execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return valid;
    }

    public User getUser(final String username){
        user = null;
        AsyncTask<String, Integer, User> getUserTask = new AsyncTask<String, Integer, User>() {
            @Override
            protected User doInBackground(String... params) {
                return RestInterface.getUser(username);
            }


        };

        try {
            user =  getUserTask.execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return user;
    }

    public void msgBox(String messageHeader, String message)
    {
        // Show popup dialog box
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_info)
                .setTitle(messageHeader)
                .setMessage(message)
                .setNeutralButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .show();
    }


}
