package net.darylbennett.guitarapp3;

/**
 * Created by Daryl on 12/11/2014.
 */
public class User {
    String email;
    String username;
    //String password;
    String phone;
    String class_id;
    String name;
    int role;
    int uid;

    //hard code time for practice
    //hard code amount of practices played (optional)

    public User() {}

    /**
     * Instantiate all the vars in constructor
     * @param email
     * @param username
     * @param phone
     * @param classId
     * @param fullName
     */
    public User(String email, String username, String phone, String classId, String fullName, int role, int uid)
    {
        this.email= email;
        this.username = username;
        this.phone = phone;
        this.class_id = classId;
        this.name = fullName;
        this.role = role;
        this.uid = uid;
    }

    public enum role {
         USER,
         ADMIN;
    }

}
