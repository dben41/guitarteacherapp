package net.darylbennett.guitarapp3;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.app.ActionBar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;


public class AdminMainActivity extends Activity
        implements AdminNavigationDrawerFragment.NavigationDrawerCallbacks {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private AdminNavigationDrawerFragment mAdminNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.admin_main_activity);

        mAdminNavigationDrawerFragment = (AdminNavigationDrawerFragment)
                getFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mAdminNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
    }


    @Override
    public void onNavigationDrawerItemSelected(int position) {
        Fragment fragment = null;
        if(position == 0) {

            fragment = new AdminMainFragment();
        } else if(position == 1) {
            Log.v("Enter", "Entering the AdminMainACtivity");
            fragment = new AdminStudentFragment();

            ((AdminStudentFragment) fragment).setStudentSelectedListener(new AdminStudentFragment.StudentSelectedListener() {
                @Override
                public void startNewStudent(int uid) {
                    // User u = AppModel.getInstance().;
                    Fragment fragment = LessonsFragment.newInstance(uid, "student");
                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.adminContainer, fragment)
                            .commit();
                }
            });

            ((AdminStudentFragment) fragment).setAddLessonListener(new AdminStudentFragment.AddLessonListener() {
                @Override
                public void addNewLesson() {
                    // User u = AppModel.getInstance().;
                    //Fragment fragment = fragmentManager.findFragmentById(R.id.add_lesson_view);
                    //LessonsFragment.newInstance(uid, "student");
                    //this is called if student submits things
                    Fragment f = new AddLessonFragment();
                    ((AddLessonFragment)f).setReturnToStudentViewListener(new AddLessonFragment.ReturnToStudentViewListener() {
                        @Override
                        public void returnEvent() {
                            FragmentManager fragmentManager = getFragmentManager();
                            fragmentManager.beginTransaction()
                                    .replace(R.id.adminContainer, new AdminStudentFragment())
                                    .commit();
                        }
                    });

                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.adminContainer, f)
                            .commit();
                }
            });


        }

            // update the main content by replacing fragments
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.adminContainer, fragment)
                    .commit();

    }


    public void onSectionAttached(int number) {
        switch (number) {
            case 1:
                mTitle = "Main Menu";
                break;
            case 2:
                mTitle = "Students";
                break;
        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mAdminNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.admin_main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch(id){
           case R.id.log_out:
                Log.d("Log out was Pressed", "true");
                final Intent main = new Intent(this, MainActivity.class);
                main.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivityForResult(main, 0);
                Toast.makeText(this, "Logged Out Successfully.", Toast.LENGTH_SHORT).show();
                return true;
        }

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
