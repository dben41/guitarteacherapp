package net.darylbennett.guitarapp3;

import android.app.Activity;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.widget.DrawerLayout;
import android.widget.Toast;


public class StudentActivity extends Activity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {
    Fragment fragment = null;
    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;
    private Intent settingsIntent;
    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));


        settingsIntent = new Intent(this, SettingsFragment.class);
        settingsIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    }


    /**
     * This is where I instantiate my views
     * @param position
     */
    @Override
    public void onNavigationDrawerItemSelected(int position) { //TODO
        Fragment fragment = null;
        if(position == 0)
            fragment = new MainMenuFragment();
        else if(position == 1) {
            User u = AppModel.getInstance().getUser();
            int uid = AppModel.getInstance().getUser().uid;
            String string = AppModel.getInstance().getUser().username;
            fragment = LessonsFragment.newInstance(uid, string);
            //fragment.set
        }
        else if (position == 2)
            fragment = new ToolsFragment();

        // update the main content by replacing fragments
        FragmentManager fragmentManager = getFragmentManager();
             fragmentManager.beginTransaction()
                   .replace(R.id.container, fragment)
                     .commit();
    }

    public void onSectionAttached(int number) {
        switch (number) {
            case 1:
                mTitle = getString(R.string.title_section1);
                break;
            case 2:
                mTitle = getString(R.string.title_section2);
                break;
            case 3:
                mTitle = getString(R.string.title_section3);
                break;
        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch(id){
            case R.id.action_settings:
                Log.d("Setting was Pressed", "true");
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.container, new SettingsFragment())
                        .commit();
                //startActivityForResult(settingsIntent, 0);
                return true;
            case R.id.action_about:
                Log.d("About was Pressed", "true");
                msgBox("About", "This app was made by Daryl Bennett. Version 1.0. \n www.DarylBennett.net");
                return true;
            case R.id.log_out:
                Log.d("Log out was Pressed", "true");
                final Intent main = new Intent(this, MainActivity.class);
                main.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivityForResult(main, 0);
                Toast.makeText(this, "Logged Out Successfully.", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

        //return super.onOptionsItemSelected(item);
        //return true;
    }


    public void msgBox(String messageHeader, String message)
    {
        // Show popup dialog box
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_info)
                .setTitle(messageHeader)
                .setMessage(message)
                .setNeutralButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .show();
    }


}
