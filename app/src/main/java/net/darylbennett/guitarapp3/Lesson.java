package net.darylbennett.guitarapp3;

/**
 * Created by Daryl on 12/17/2014.
 */
public class Lesson {

    String date;
    String content;
    String username;
    int uid;

    //hard code time for practice
    //hard code amount of practices played (optional)

    public Lesson() {}

    /**
     * Instantiate all the vars in constructor
     */
    public Lesson(String username, String content, String date, int uid)
    {
        this.date= date;
        this.content = content;
        this.username = username;
        this.uid = uid;
    }



}
