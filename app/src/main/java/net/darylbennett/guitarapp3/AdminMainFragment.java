package net.darylbennett.guitarapp3;

import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.concurrent.ExecutionException;

/**
 * Created by Daryl on 12/15/2014.
 */
public class AdminMainFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.admin_main_fragment, container, false);

        //name of admin
        TextView adminName = (TextView) rootView.findViewById(R.id.adminNameText);
        adminName.setText("Name: " + AppModel.getInstance().getUser().name);

        //name of class
        TextView className = (TextView) rootView.findViewById(R.id.class_number);
        className.setText("Class Id: " + AppModel.getInstance().getUser().class_id );

        //number of students
        TextView classSize = (TextView) rootView.findViewById(R.id.class_size);
        classSize.setText("Number of students: " + getStudentCount(AppModel.getInstance().getUser().class_id));

        //(optional) current date, next lesson, calendar view

        return rootView;
    }

    public int getStudentCount(final String classId) {
        int count = 0;
        AsyncTask<String, Integer, Integer> getStudentCount = new AsyncTask<String, Integer, Integer>() {

            @Override
            protected Integer doInBackground(String... strings) {
                return RestInterface.classCount(classId);
            }
        };

        try {
            count = getStudentCount.execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return count;
    }
}
