package net.darylbennett.guitarapp3;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        //set the view
        setContentView(R.layout.activity_test);

        //change the font
        Typeface type = Typeface.createFromAsset(getAssets(), "fonts/downward_fall.ttf");
        TextView tv = (TextView) findViewById(R.id.title);
        tv.setTypeface(type);

        //declare the buttons
        final Button signInButton = (Button)findViewById(R.id.signin_button);
        final Button registerButton = (Button)findViewById(R.id.register_button);

        //set up intent
        final Intent loginIntent = new Intent(this, LoginActivity.class);
        loginIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        final Intent registerIntent = new Intent(this, RegisterUser.class);
        registerIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        //Set the on click listener
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(loginIntent, 0);
            }
        });

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(registerIntent, 0);
            }
        });

    }
}