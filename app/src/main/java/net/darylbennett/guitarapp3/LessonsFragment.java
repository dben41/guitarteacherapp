package net.darylbennett.guitarapp3;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.database.DataSetObserver;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

/**
 * Created by Daryl on 12/11/2014.
 */
public class LessonsFragment extends Fragment implements ListAdapter {

    ArrayList<Lesson> allLesson;
    ListView lessonListView;
    int uid;
    String username;
    public LessonsFragment(){}

    public static final LessonsFragment newInstance(int uid, String username)
    {
        LessonsFragment l = new LessonsFragment();
        Bundle bundle = new Bundle(2);
        bundle.putInt("uid", uid);
        bundle.putString("username", username);
        l.setArguments(bundle);

        return l;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //get the uid for this fragment
        uid = getArguments().getInt("uid");
        username = getArguments().getString("username");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //contains all the views
        LinearLayout rootLayout= new LinearLayout(getActivity());
        rootLayout.setOrientation(LinearLayout.VERTICAL);
        //Tell the admin info about this view. In the future, add users here? show number of students?
        TextView infoView = new TextView(getActivity());
        infoView.setText("Displaying records for: " + username + ".");
        infoView.setBackgroundColor(Color.rgb(0x23, 0xB5, 0xE5));

        //instantiate the listView and set the adapter0
        lessonListView = new ListView(getActivity());
        lessonListView.setAdapter(this);

        //add the views to the layout and return
        rootLayout.addView(infoView, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0, 15));
        rootLayout.addView(lessonListView, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0, 85));
        return rootLayout;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return true;
    }

    @Override
    public boolean isEnabled(int i) {
        return false;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver dataSetObserver) {

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver dataSetObserver) {

    }

    @Override
    public int getCount() {
        //get all the lessons
        allLesson = getLessons(uid);
        if(allLesson != null)
            return allLesson.size();
        else
            return 0;
    }

    @Override
    public Object getItem(int i) {
        return allLesson.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    /**
     * Prolly add an edit button or add note feature in future releases
     */
    public View getView(int i, View view, ViewGroup viewGroup) {
        int lessonCount = this.getCount();
        final int count = i;
        LinearLayout root = new LinearLayout((getActivity()));
        root.setOrientation(LinearLayout.VERTICAL);

        //why would the user need to know their own username?
        //TextView username = new TextView(getActivity());
        //username.setText(allLesson.get(i).username);

        TextView content = new TextView(getActivity());
        String temp = allLesson.get(i).content;
        content.setText(allLesson.get(i).content);

        TextView date = new TextView(getActivity());
        SpannableString spanString = new SpannableString(allLesson.get(i).date);
        spanString.setSpan(new StyleSpan(Typeface.ITALIC),0,spanString.length(),0);
        date.setText(spanString);

        root.addView(date);
        root.addView(content);

//        if(count == 0){
//            TextView defaultMsg = new TextView(getActivity());
//            defaultMsg.setText("There are no lessons to display.");
//            root.addView(defaultMsg);
//        }

        root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.v("Item " + count , "This item was touched!");
            }
        });

        return root;
    }

    @Override
    public int getItemViewType(int i) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean isEmpty() {
        return this.getCount()<=0;
    }

    public ArrayList<Lesson> getLessons(final int uid){
        ArrayList<Lesson> lessons;
        RestInterface.LessonList lessonsList = null;
        lessons = null;
        AsyncTask<String, Integer, RestInterface.LessonList> restGetLessons = new AsyncTask<String, Integer, RestInterface.LessonList>() {
            @Override
            protected RestInterface.LessonList doInBackground(String... params) {
                return RestInterface.getLessonList(uid);
            }
        };

        try {
            lessonsList =  restGetLessons.execute().get();
            lessons = new ArrayList<Lesson>();
            if(lessonsList != null)
            for(RestInterface.Lesson temp : lessonsList.lessons){
                Lesson tempLesson = new Lesson();
                tempLesson.username = this.username;
                tempLesson.content = temp.content;
                tempLesson.date = temp.date;
                tempLesson.uid = temp.uid;
                lessons.add(tempLesson);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return lessons;
    }



}
