package net.darylbennett.guitarapp3;

import android.app.Fragment;
import android.database.DataSetObserver;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

/**
 * Created by Daryl on 12/14/2014.
 */
public class LessonListFragment extends Fragment implements ListAdapter {
    //underlying structure to hold the data
    private Set<UUID> lessons = new HashSet<UUID>();

    //empty constructor
    public LessonListFragment(){}

    //declare screen view
    ListView lessonListView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getUsers("g");
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public boolean areAllItemsEnabled() {
        return true;
    }

    @Override
    public boolean isEnabled(int position) {
        return true;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        int i = this.getCount();
        return null;
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean isEmpty() {
        return getCount()<=0;
    }


    /**
     * This creates a user
     * @param username
     * @param password
     * @return
     */
    RestInterface.UserList userListl;
    public RestInterface.UserList getUsers(final String classId){
        userListl = null;
        AsyncTask<String, Integer, RestInterface.UserList> createUser = new AsyncTask<String, Integer, RestInterface.UserList>() {
            @Override
            protected RestInterface.UserList doInBackground(String... params) {
                return RestInterface.getClassUsers(classId);
            }

            @Override
            protected void onPostExecute(RestInterface.UserList userList) {
                super.onPostExecute(userList);
                userListl = userList;
            }
        };

        try {
            userListl =  createUser.execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return userListl;
    }
}
